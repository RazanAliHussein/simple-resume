// // back/src/db.js
// import sqlite from 'sqlite'
// import SQL from 'sql-template-strings';

// const test = async () => {

//   const db = await sqlite.open('./db.sqlite');
//   /**
//    * Create the table booking
//    **/ 
//   await db.run(`CREATE TABLE Bookings (
//     booking_id integer NOT NULL CONSTRAINT Bookings_pk PRIMARY KEY AUTOINCREMENT,
//     dates date NOT NULL,
//     time datetime NOT NULL,
//     email text NOT NULL,
//     name text NOT NULL,
//     phone integer NOT NULL,
//     details clob NOT NULL,
//     location text NOT NULL
// );`);
//  /**
//    * Create the table testemonials
//    **/ 
// await db .run(`CREATE TABLE Testemonials (
//     testemonials_id integer NOT NULL CONSTRAINT Testemonials_pk PRIMARY KEY AUTOINCREMENT,
//     author text NOT NULL,
//     descreption clob NOT NULL,
//     image blob NOT NULL
// );`);
//  /**
//    * Create the table posts
//    **/ 

// await db.run(`CREATE TABLE posts (
//     post_id integer NOT NULL CONSTRAINT posts_pk PRIMARY KEY AUTOINCREMENT,
//     dates date NOT NULL,
//     title text NOT NULL,
//     description clob NOT NULL,
//     image blob NOT NULL
// );`);
//   /**
//    * let's insert a bit of data in it. We're going to insert 10 users
//    * We first create a "statement"
//    **/
//    const stmt = await db.prepare(SQL`INSERT INTO posts (dates, title,description,image) VALUES (?, ?,?,?)`);
//   let i = 0;
//    while(i<10){
//     await stmt.run(`${i}/${i}/${i}${i}${i}${i}`,`post ${i}`,"In descriptive writing, the author does not tell the reader what was seen, felt, tested, smelled, or heard. Rather, he describes something that he experienced and, through his choice of words, makes it seem real. In other words, descriptive writing is vivid, colorful, and detailed. ",`image${i}`);
//     i++
//   }
//   /** finally, we close the statement **/
//      await stmt.finalize();

// //   /**
// //    * Then, let's read this data and display it to make sure everything works
// //    **/
//   const rows = await db.all("SELECT post_id AS id, dates, title,description,image FROM posts")
//   rows.forEach( ({ id, dates, title,description,image }) => console.log(`[id:${id}] - ${dates} - ${title} -${description} -${image}`) )
// }

// export default { test }
// back/src/db.js
import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {
  const nowForSQLite = () => new Date().toISOString().replace('T',' ').replace('Z','')
  const db = await sqlite.open('./db.sqlite');
  const ensurePropertiesExist = (obj, names) => {
    if(!obj){ 
      throw new Error(`the properties `+names.join(',')+' are necessary')
    }
    for(let i=0; i < names.length; i++){
      const propertyName = names[i]
      if(obj[propertyName] === null || typeof obj[propertyName] === 'undefined' ){
        throw new Error(`property "${propertyName}" is necessary`)
      }
    }
    return obj
  }
  
  /**
   * retrieves the contacts from the database
   */
  const getpostsList = async () => {
    const rows = await db.all("SELECT post_id AS id, dates, title,description,image FROM posts")
    return rows
  }
    /**
   * creates a post
   * @param {object} props an object with keys `dates` and `title`,`description`,`image`
   * @returns {number} the id of the created  (or an error if things went wrong) 
   */
   //Update Blog
   const updatePost = async (id, props) => 
   { const { dates, title, description ,image} = props 
   const result = await db.run(SQL `UPDATE posts SET dates=${dates}, title=${title}, description=${description},image=${image} WHERE post_id = ${id}`); 
   if(result.stmt.changes === 0)
   { return false }
    return true }
    //update blog end
  const createPost = async (props) => {
    if(!props || !props.dates || !props.title|| !props.description || !props.image){
      throw new Error(`you must provide all properties`)
    }
    const { dates, title, description,image } = props
    try{
      const result = await db.run(SQL`INSERT INTO posts (dates, title, description,image) VALUES (${dates}, ${title},${description},${image})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
   /**
   * creates a Testemonials
   * @param {object} props an object with keys `author`,`description`,`image`
   * @returns {number} the id of the created  (or an error if things went wrong) 
   */
  const createTestemonials = async (props) => {
    if(!props || !props.author || !props.description|| !props.image){
      throw new Error(`you must provide all properties`)
    }
    const { author, description,image } = props
    try{
      const result = await db.run(SQL`INSERT INTO Testemonials (author,description,image) VALUES (${author},${description},${image})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
 //Update Blog
 const updateTestimonial = async (id, props) => 
 { const { author,description ,image} = props 
 const result = await db.run(SQL `UPDATE Testemonials SET author=${author}, description=${description},image=${image} WHERE testemonials_id = ${id}`); 
 if(result.stmt.changes === 0)
 { return false }
  return true }
  //update blog end
    /**
   * creates a booking
   * @param {object} props an object with keys `dates` ,`time`,`email`,`name`,`time`,`phones`,`details`,`location`
   * @returns {number} the id of the created booking (or an error if things went wrong) 
   */
  const createBooking = async (props) => {
    if(!props || !props.dates || !props.time || !props.email || !props.name || !props.phones || !props.details || !props.location){
      throw new Error(`you must provide a property`)
    }
    const { dates,time,email,name,phones,details,location } = props
    try{
      const result = await db.run(SQL`INSERT INTO Bookings (dates,time,email,name,phones,details,location ) VALUES (${dates}, ${time},${email},${name},${phones},${details},${location})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
 /**
   * deletes a posts
   * @param {number} id the id of the post to delete
   * @returns {boolean} `true` if the post was deleted, an error otherwise 
   */
  const deletePost = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM posts WHERE post_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`post "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the post "${id}": `+e.message)
    }
  }
  /**
   * deletes a testemonials
   * @param {number} id the id of the testemonials to delete
   * @returns {boolean} `true` if the post was deleted, an error otherwise 
   */
  const deleteTestemonials = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM Testemonials WHERE testemonials_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`Testemonials "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the Testemonials "${id}": `+e.message)
    }
  }
  /**
   * deletes a Booking
   * @param {number} id the id of the booking to delete
   * @returns {boolean} `true` if the booking was deleted, an error otherwise 
   */
  const deleteBooking = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM Bookings WHERE booking_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`Booking "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the booking "${id}": `+e.message)
    }
  }
   /**
   * Retrieves a Testemonials
   * @param {number} id the id of the testemonials
   * @returns {object} an object with `author`,`description`,`image`, representing a testemonial, or an error 
   */
  const getTestemonial = async (id) => {
    try{
      const testemonialsList = await db.all(SQL`SELECT testemonials_id AS id,author,description,image FROM Testemonials WHERE testemonials_id = ${id}`);
      const contact = testemonialsList[0]
      if(!contact){
        throw new Error(`testemonial ${id} not found`)
      }
      return contact
    }catch(e){
      throw new Error(`couldn't get the testemonial ${id}: `+e.message)
    }
  }
/**
   * Retrieves a posts
   * @param {number} id the id of the posts
   * @returns {object} an object with `dates`,`description title`,`image`, representing a testemonial, or an error 
   */
  const getPost = async (id) => {
    try{
      const postList = await db.all(SQL`SELECT post_id AS id, dates, description,title,image FROM posts WHERE post_id = ${id}`);
      const contact = postList[0]
      if(!contact){
        throw new Error(`post ${id} not found`)
      }
      return contact
    }catch(e){
      throw new Error(`couldn't get the post ${id}: `+e.message)
    }
  }
/**
   * Retrieves a Booking
   * @param {number} id the id of the booking
   * @returns {object} an object with `dates`,`time`,`email`,`name`,`phone details location`, representing a booking, or an error 
   */
  const getBooking = async (id) => {
    try{
      const bookingList = await db.all(SQL`SELECT booking_id AS id, dates, time,email,name,phone,details,location FROM Bookings WHERE booking_id = ${id}`);
      const contact = bookingList[0]
      if(!contact){
        throw new Error(`booking ${id} not found`)
      }
      return contact
    }catch(e){
      throw new Error(`couldn't get the booking ${id}: `+e.message)
    }
  }
 /**
   * retrieves the posts from the database
   * @param {string} orderBy an optional string that is either "dates" "title"
   * @returns {array} the list of contacts
   */
  const getPostsList = async (orderBy) => {
    try{
      
      let statement = `SELECT post_id AS id, title, description,dates,image FROM posts`
      switch(orderBy){
        case 'dates': statement+= ` ORDER BY dates`; break;
        case 'title': statement+= ` ORDER BY title`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve contacts: `+e.message)
    }
  }
  /**
   * retrieves the testemonial from the database
   * @param 
   * @returns {array} the list of contacts
   */
  const getTestemonialList = async () => {
    try{
      
      let statement = `SELECT testemonials_id AS id, author, description,image FROM Testemonials`
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve contacts: `+e.message)
    }
  }
 /**
   * retrieves the booking from the database
   * @param {string} orderBy an optional string that is either "date" 
   * @returns {array} the list of contacts
   */
  const getBookingList = async (orderBy) => {
    try{
      
      let statement = `SELECT booking_id AS id, dates,time,email,name,phone,details,location FROM Bookings`
      switch(orderBy){
        case 'dates': statement+= ` ORDER BY dates`; break;
     
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve bookings: `+e.message)
    }
  }


  const controller = {
    getpostsList,
    createPost,
    createTestemonials,
    createBooking,
    deletePost,
    deleteTestemonials,
    deleteBooking,
    getTestemonial,
    getPost,
    getBooking,
    getPostsList,
    getTestemonialList,
    getBookingList,
    updatePost,
    updateTestimonial
  }

  return controller
}

export default initializeDatabase
