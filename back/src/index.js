// back/src/index.js
import app from './app'
import initializeDatabase from './db'
import { authenticateUser, logout, isLoggedIn } from './auth'
import multer from 'multer'
import path from 'path'
var nodemailer = require("nodemailer");
//image
const multerStorage = multer.diskStorage({
  destination: path.join( __dirname, '../public/images'),
  filename: (req, file, cb) => {
    const { fieldname, originalname } = file
    const date = Date.now()
    // filename will be: image-1345923023436343-filename.png
    const filename = `${fieldname}-${date}-${originalname}` 
    cb(null, filename)
  }
})
const upload = multer({ storage: multerStorage  })

const start = async () => {
  const controller = await initializeDatabase()
  
  app.get('/', (req, res, next) => res.send("ok"));

  // CREATE POst
  app.post('/post/new',upload.single('image'), async (req, res, next) => {
    try{
    const { dates,description,title } = req.query
    //  console.log(req)
    const image = req.file && req.file.filename
   
    const result = await controller.createPost({dates,description,title,image})
    res.json({success:true, result})
   
    }
    catch(e){
      next(e)
    }
  
  })

  // READ Post
  app.get('/post/get/:id', async (req, res, next) => {
    try{
    const { id } = req.params
    const contact = await controller.getPost(id)
    res.json({success:true, result:contact})
  }
    catch(e){
      next(e)
    }
  })

  // DELETE post
  app.get('/post/delete/:id', async (req, res, next) => {
    try{
    const { id } = req.params
    const result = await controller.deletePost(id)
    res.json({success:true, result})
  }
    catch(e){
      next(e)
    }
  })

  // UPDATE post
  app.get('/post/update/:id', async (req, res, next) => {
    const { id } = req.params
   const { dates, title, description,image } = req.query
    const result = await controller.updatePost(id,{dates,title,description,image})
     res.json({success:true, result})
  })

  // LIST post
  app.get('/post/list', async (req, res, next) => {
    try{
    const { order } = req.query
    const contacts = await controller.getPostsList(order)
    res.json({success:true, result:contacts})
  }
  catch(e){
    next(e)
  }
  })
  /********************************************************************************/
 // CREATE testemonial
 app.post('/testemonial/new',upload.single('image'), async (req, res, next) => {
   try{
  const { author,description} = req.query
  const image = req.file && req.file.filename
   
  const result = await controller.createTestemonials({author,description,image})
  res.json({success:true, result})
}
catch(e){
  next(e)
}
})

// READ testemonial
app.get('/testemonial/get/:id', async (req, res, next) => {
  try{
  const { id } = req.params
  const contact = await controller.getTestemonial(id)
  res.json({success:true, result:contact})
}
catch(e){
  next(e)
}
})

// DELETE testemonial
app.get('/testemonial/delete/:id', async (req, res, next) => {
  try{
  const { id } = req.params
  const result = await controller.deleteTestemonials(id)
  res.json({success:true, result})
}
catch(e){
  next(e)
}
})

// UPDATE testemonial
app.get('/testemonial/update/:id', async (req, res, next) => {
  const { id } = req.params
  const { author, description,image } = req.query
   const result = await controller.updateTestimonial(id,{author,description,image})
   res.json({success:true, result})
})

// LIST testemonial
app.get('/testemonial/list', async (req, res, next) => {
try{
  const contacts = await controller.getTestemonialList()
  res.json({success:true, result:contacts})
}
catch(e){
  next(e)
}
})
/********************************************************************************/
 // CREATE booking
 app.get('/booking/new', async (req, res, next) => {
   try{
  const { dates,time,email,name,phone,details,location } = req.query
  const result = await controller.createBooking({dates,time,email,name,phone,details,location})
  res.json({success:true, result})
}
catch(e){
  next(e)
}
})

// READ booking
app.get('/booking/get/:id', async (req, res, next) => {
  try{
  const { id } = req.params
  const contact = await controller.getBooking(id)
  res.json({success:true, result:contact})
}
catch(e){
  next(e)
}
})

// DELETE booking
app.get('/booking/delete/:id', async (req, res, next) => {
  try{
  const { id } = req.params
  const result = await controller.deleteBooking(id)
  res.json({success:true, result})
}
catch(e){
  next(e)
}
})

// UPDATE booking
app.get('/booking/update/:id',async (req, res, next) => {
  // const { id } = req.params
  // const { name, email } = req.query
  // const result = await controller.updateContact(id,{name,email})
  // res.json({success:true, result})
})

// LIST booking
app.get('/booking/list', async (req, res, next) => {
  try{
  const { order } = req.query
  const contacts = await controller.getBookingList(order)
  res.json({success:true, result:contacts})
}
catch(e){
  next(e)
}
})
/********************************************************************************/
  // ERROR
  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({ success:false, message })
  })
//authentication
//app.get('/login', authenticateUser)
  
//app.get('/logout', logout)

app.get('/mypage', isLoggedIn, ( req, res ) => {
  const username = req.user.name
  res.send({success:true, result: 'ok, user '+username+' has access to this page'})
})

  app.listen(8080, () => console.log('server listening on port 8080'))

/////////email
  
  app.post("/send", function(req, res, next) {
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "hannahbethcooper2@gmail.com",
        pass: "hannah@123"
      }
    });
    const mailOptions = {
      from: `${req.body.email}`,
      to: "hannahbethcooper2@gmail.com",
      subject: `${req.body.name}`,
      text: `${req.body.message}`,
      replyTo: `${req.body.email}`
    };
    transporter.sendMail(mailOptions, function(err, res) {
      if (err) {
        console.error("there was an error: ", err);
      } else {
        console.log("here is the res: ", res);
      }
    });
  });

}



start()
