import React, { Component } from "react";
import { withRouter } from "react-router";

//authentication
import * as auth0Client from "./auth";
import { pause, makeRequestUrl } from "./utils.js";
//CSS
import $ from "jquery";
import "./App.css";
import "./Components/Blog.css";
//components
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import About from "./Components/About";
import Resume from "./Components/Resume";
import Contact from "./Components/Contact";
import Testimonials from "./Components/Testimonials";
import Portfolio from "./Components/Portfolio";
import Blog from "./Components/Blog";
import Dash from "./dashboard/pages";
//interaction
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
//authentication
const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

// const AuthRoute = ({exact, path, component}) => <Route exact={exact} path={path} render={()=>{
//   if(auth0Client.isAuthenticated()){
//     const Comp = component
//     return <Comp/>
//   }
//   return <div>{this.renderUser}</div>
// }} />

class App extends Component {
  state = {
    posts_list: [],
    foo: "bar",
    resumeData: {},
    isLoading: false,
    checkingSession: true
  };
  //Ajax
  getResumeData() {
    $.ajax({
      url: "/resumeData.json",
      dataType: "json",
      cache: false,
      success: function(data) {
        this.setState({ resumeData: data });
      }.bind(this),
      error: function(xhr, status, err) {
        console.log(err);
        alert(err);
      }
    });
  }
  //Authentication Stuff
  renderUser() {
    const isLoggedIn = auth0Client.isAuthenticated();
    if (isLoggedIn) {
      // user is logged in
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return <button onClick={auth0Client.signIn}>Sign In</button>;
  }
  renderUserLoggedIn() {
    const nick = auth0Client.getProfile().name;
    return (
      <div>
        <Dash/>
        Hello, {nick}! <button onClick={auth0Client.signOut}>logout</button>
        
      </div>
    );
  }

  getPersonalPageData = async () => {
    try {
      const url = makeUrl(`mypage`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        const message = answer.result;
        // we should see: "received from the server: 'ok, user <username> has access to this page'"
   
        toast(`received from the server: '${message}'`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(
          `error message received from the server: ${answer.message}`
        );
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  isLogging = false;
  login = async () => {
    if (this.isLogging === true) {
      return;
    }
    this.isLogging = true;
    try{
      await auth0Client.handleAuthentication();
      const name = auth0Client.getProfile().name
       // get the data from Auth0
       console.log(name)
      if(name==="hannah@gmail.com"){
      await this.getPersonalPageData() // get the data from our server
      toast(`${name} is logged in`)
  
    this.props.history.push('/dashboard')}
    else{
      toast("cant")
    auth0Client.signOut();
    }
    }catch(err){
      this.isLogging = false
      toast.error(err.message);
    }
  }
  handleAuthentication = ({history}) => {
    console.log(this.props)
    this.login(history)
    return <p>wait...</p>
  }

  
  //component Didmount
  async componentDidMount() {
    this.getResumeData();
   console.log(this.props)
    if (this.props.location.pathname === '/callback'){
      return
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData(); // get the data from our server
      this.forceUpdate();
    } catch (err) {
      if (err.error !== 'login_required'){
        console.log(err.error);
      }
    }
    
  
    
  }
  render() {
    return (
      <Router>
        <div className="App">
          <Route
            exact
            path="/dashboard"
            render={() => {
              if (auth0Client.isAuthenticated()) {
                if(auth0Client.getProfile().name=="test")
                return <Dash />;
              }
              return <div>{this.renderUser()}</div>;
            }}
          />
          <Route path="/callback" render={this.handleAuthentication} />
          <ToastContainer />
          <Route
            exact
            path="/"
            render={props => (
              <div>
                <Header data={this.state.resumeData.main} />
                <About data={this.state.resumeData.main} />
                <Resume data={this.state.resumeData.resume} />
                <Portfolio data={this.state.resumeData.portfolio} />
                <Blog />
                <Testimonials data={this.state.resumeData.testimonials} />
                <Contact data={this.state.resumeData.main} />
                <Footer data={this.state.resumeData.main} />
              </div>
            )}
          />
          <Route path="/blog" component={Blog}/>
        </div>
      </Router>
    );
  }
}

export default withRouter(App);
