import React, { Component } from "react";
import Blogdash from "./blogdash";
import Testimonialdash from "./testimonialdash";
import "./dash.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


import * as auth0Client from "../auth";
import SecuredRoute from '../SecuredRoute';



class Pages extends Component {
  state = {
    select: "-",
    checkingSession:false
  };
  handleChange = e => {
    this.setState({ select: e.target.value });
  };
  renderPages = () => {
    if (this.state.select === "blog") {
      return (<Blogdash />);
    } else if (this.state.select === "testimonial") {
      return (<Testimonialdash />);
    } else {
      return <div>Please Choose which page you wanna update</div>;
    }
  };

  componentDidMount() {
    toast("Now You Can Change Your Website Data");
   
  }
  // renderUser() {
  //   const isLoggedIn = auth0Client.isAuthenticated();
  //   if (isLoggedIn) {
  //     // user is logged in
  //     return this.renderUserLoggedIn();
  //   } else {
  //     return this.renderUserLoggedOut();
  //   }
  // }
  // renderUserLoggedOut() {
  //   return <button onClick={auth0Client.signIn}>Sign In</button>;
  // }
  // renderUserLoggedIn() {
  //   const nick = auth0Client.getProfile().name;
  //   return (
  //     <div>
  //       Hello, {nick}!{" "}
  //       <button onClick={()=>{auth0Client.signOut();this.setState({})}}>
  //         logout
  //       </button>
  //     </div>
  //   );
  // }

  render() {
    console.log(this.state.select);
    return (
      <div className="Pages">
        <ToastContainer />
        <h1>Hi Hannah </h1>
        <aside className="asideMenu menuItem">
          <label className="pagechoice">Choose the page:</label>
          <br />
          <br />
          <select className="menuItem" onChange={this.handleChange}>
            <option>--</option>
            <option value="blog">Blog</option>
            <option value="testimonial">Testimonial</option>
          </select>
        </aside>
        {this.renderPages()}
      </div>
    );
  }
}
export default Pages;
