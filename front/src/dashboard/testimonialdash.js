import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as auth0Client from "../auth.js";
class Testimonialdash extends Component {
  state = {
    createvisibility: false,
    editvisibility: false,
    testimonial_list: [],
    author: "",
    description: "",
    image: ""
  };
  togglecreatevisibility = () => {
    this.setState({
      createvisibility: !this.state.createvisibility
    });
  };
  toggleeditvisibility = id => {
    this.setState({ id: id }, () => {
      this.setState(
        {
          editvisibility: !this.state.editvisibility
        },
        () => {
          this.getTestimonial(id);
          toast("edit it above")
        }
      );
    });
  };
  updateTestimonial = async (id, props) => {
    try {
      if (
        !props ||
        !(props.author || props.description || props.image)
      ) {
        throw new Error(`you need at least one property`);
      }
      const response = await fetch(
        `http://localhost:8080/testemonial/update/${id}?author=${props.author}
        &description=${props.description}&image=${props.image}`
      );
      console.log(response);
      const answer = await response.json();
      console.log(answer);
      if (answer) {
        // we update the user, to reproduce the database changes:
        const testimonial_list = this.state.testimonial_list;
        testimonial_list.map(member => {
          // if this is the member we need to change, update it. This will apply to exactly
          // one member
          if (member.id === id) {
            const new_member = {
              id: member.member_id,
              author: props.author,
              description: props.description,
              image: props.image,
             
            };

            return new_member;
          }
          // otherwise, don't change the members at all
          else {
            return member;
          }
        });
        this.setState({ testimonial_list });
        toast("updated")
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getTestimonial = async id => {
    const previous_product = this.state.testimonial_list.find(
      product => product.post_id === id
    );
    if (previous_product) {
      return; // do nothing, no need to reload a contact we already have
    }
    try {
      const response = await fetch(
        `http://localhost:8080/testemonial/get/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const product = answer.result;
        console.log(product);
        const author = product.author;
        this.setState({ author });
        const description = product.description;
        this.setState({ description });
        const image = product.image;
        this.setState({ image });

        const testimonial_list = [...this.state.testimonial_list, product];
        this.setState({ testimonial_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  CreateTestimonial = async props => {
    try {
      if (!props || !(props.author && props.description)) {
        throw new Error(`you need to fill all the fields`);
      }
      let body = null;
      if (props.image) {
        body = new FormData();
        body.append(`image`, props.image);
      }

      const { author, description, image } = props;
      const response = await fetch(
        `http://localhost:8080/testemonial/new?author=${author}&description=${description}`,
        {
          method: 'POST',
          body
        }
      );

      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        //const id = answer.result;
        const product = { author, description };
        const testimonial_list = [...this.state.testimonial_list, product];
        this.setState({ testimonial_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  deleteTestimonial = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/testemonial/delete/${id}`
      );

      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const testimonial_list = this.state.testimonial_list.filter(
          product => product.id !== id
        );
        this.setState({ testimonial_list });
        toast("Testimonial deleted");
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getTestimonialList = async () => {
    try {
      const response = await fetch(`http://localhost:8080/testemonial/list`);
      const answer = await response.json();
      if (answer.success) {
        const testimonial_list = answer.result;
        this.setState({ testimonial_list });
      } else {
        const error_message = answer.message;
        this.setState({ error_message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  onSubmit = evt => {
    evt.preventDefault()
    // extract name and email from state
    const { author, description } = this.state;
    //console.log(title, description, price, category_id);
    // create the contact from mail and email
    const image = evt.target.filefield.files[0];
    this.CreateTestimonial({ author, description, image });
    // empty name and email so the text input fields are reset
    this.setState({ author: "", description: "" });
    toast("New Row Inserted");
  };
  componentDidMount() {
    this.getTestimonialList();
  }
  render() {
    return (
      <div className="Testimonialdash">
       <div className="Editmode">
          {this.state.editvisibility ? (
            <div>
              {/* <form onSubmit={()=>{this.CreatePost()}}> */}
              <label>author</label>
              <input
                type="text"
                placeholder="author "
                onChange={evt => this.setState({ author: evt.target.value })}
                value={this.state.author}
              />
              <label>description</label>
              <textarea
                id="Description"
                name="Description"
                placeholder="Write something.."
                style={{ height: 200 }}
                onChange={evt =>
                  this.setState({ description: evt.target.value })
                }
                value={this.state.description}
              />
              <label>image</label>
              <input
                type="file"
                placeholder="image "
                onChange={evt => this.setState({ image: evt.target.value })}
               // value={this.state.image}
              />
              <input
                className="col-75"
                type="submit"
                value="Submit"
                onClick={() => this.updateTestimonial(this.state.id,{author:this.state.author,description:this.state.description,image:this.state.image})}
              />
            </div>
          ) : null}
        </div>
      <div className="record">
        <button onClick={() => this.togglecreatevisibility()} >New Record</button>
        {this.state.createvisibility ? (
          <div>
              <form onSubmit={this.onSubmit}>
            <label>author</label>
            <input
              type="text"
              placeholder="autor "
              onChange={evt => this.setState({ author: evt.target.value })}
            />
            <label>description</label>
            <textarea
              id="Description"
              name="Description"
              placeholder="Write something.."
              style={{ height: 200 }}
              onChange={evt => this.setState({ description: evt.target.value })}
            />
            <label>image</label>
            <input
               type="file"
               name="filefield"
               onChange={evt => this.setState({ image: evt.target.value })}
              
            />
            <input
              className="col-75"
              type="submit"
              value="Submit"
             // onClick={() => this.onSubmit()}
            />
            </form>
          </div>
        ) : null}
        </div>
        {this.state.testimonial_list.map(test => {
          return (
            <div>
              <ToastContainer />
              <table>
                <tr>
                  <td>{test.id}</td>
                  <td>{test.author}</td>
                  <td>{test.description}</td>
                  <td>{test.image}</td>
                  <td>
                    <button onClick={() => this.deleteTestimonial(test.id)}>
                      delete
                    </button>
                  </td>
                  <td>
                    <button onClick={()=>this.toggleeditvisibility(test.id)}>Edit</button>
                  </td>
                </tr>
              </table>
            </div>
          );
        })}
      </div>
    );
  }
}
export default Testimonialdash;
