import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as auth0Client from "../auth.js";
class Blogdash extends Component {
  state = {
    createvisibility: false,
    editvisibility: false,
    articles_list: [],
    id: "",
    description: "",
    title: "",
    image: "",
    dates: ""
  };
  togglecreatevisibility = () => {
    this.setState({
      createvisibility: !this.state.createvisibility
    });
  };
  toggleeditvisibility = id => {
    this.setState({ id: id }, () => {
      this.setState(
        {
          editvisibility: !this.state.editvisibility
        },
        () => {
          this.getPost(id);
          toast("edit it above");
        }
      );
    });
  };
  deletePost = async id => {
    try {
      const response = await fetch(`http://localhost:8080/post/delete/${id}`);

      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const articles_list = this.state.articles_list.filter(
          product => product.id !== id
        );
        this.setState({ articles_list });
        toast("Post deleted");
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getArticleList = async () => {
    try {
      const response = await fetch(`http://localhost:8080/post/list`);
      const answer = await response.json();
      if (answer.success) {
        const articles_list = answer.result;
        this.setState({ articles_list });
      } else {
        const error_message = answer.message;
        this.setState({ error_message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  CreatePost = async props => {
    try {
      if (!props || !(props.title && props.description && props.dates)) {
        throw new Error(`you need to fill all the fields`);
      }
      let body = null;
      if (props.image) {
        body = new FormData();
        body.append(`image`, props.image);
      }

      const { dates, description, title, image } = props;
      const response = await fetch(
        `http://localhost:8080/post/new?dates=${dates}&description=${description}&title=${title}`,
        {
          method: "POST",
          body
        }
      );

      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        //const id = answer.result;
        const product = { dates, description, title };
        const articles_list = [...this.state.articles_list, product];
        this.setState({ articles_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updatePost = async (id, props) => {
    try {
      if (
        !props ||
        !(props.title || props.description || props.image || props.dates)
      ) {
        throw new Error(`you need at least one property`);
      }
      const response = await fetch(
        `http://localhost:8080/post/update/${id}?dates=${props.dates}&title=${
          props.title
        }&description=${props.description}&image=${props.image}`
      );
      console.log(response);
      const answer = await response.json();
      console.log(answer);
      if (answer) {
        // we update the user, to reproduce the database changes:
        const articles_list = this.state.articles_list;
        articles_list.map(member => {
          // if this is the member we need to change, update it. This will apply to exactly
          // one member
          if (member.id === id) {
            const new_member = {
              id: member.member_id,
              title: props.title,
              description: props.description,
              image: props.image,
              dates: props.dates
            };

            return new_member;
          }
          // otherwise, don't change the members at all
          else {
            return member;
          }
        });
        this.setState({ articles_list });
        toast("updated");
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getPost = async id => {
    const previous_product = this.state.articles_list.find(
      product => product.post_id === id
    );
    if (previous_product) {
      return; // do nothing, no need to reload a contact we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/post/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const product = answer.result;
        console.log(product);
        const dates = product.dates;
        this.setState({ dates });
        const title = product.title;
        this.setState({ title });
        const description = product.description;
        this.setState({ description });
        const image = product.image;
        this.setState({ image });

        const articles_list = [...this.state.articles_list, product];
        this.setState({ articles_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  componentDidMount() {
    this.getArticleList();
  }
  onSubmit = evt => {
    evt.preventDefault();
    // extract name and email from state
    const { dates, description, title } = this.state;
    //console.log(title, description, price, category_id);
    // create the contact from mail and email
    const image = evt.target.filefield.files[0];

    this.CreatePost({ dates, description, title, image });
    // empty name and email so the text input fields are reset
    this.setState({ title: "", dates: "", description: "" });
    toast("New Row Inserted");
  };
  render() {
    return (
      <div className="Blogdash">
        <div className="Editmode">
          {this.state.editvisibility ? (
            <div>
              {/* <form onSubmit={()=>{this.CreatePost()}}> */}
              <label>date</label>
              <input
                type="text"
                placeholder="date "
                onChange={evt => this.setState({ dates: evt.target.value })}
                value={this.state.dates}
              />
              <label>description</label>
              <textarea
                id="Description"
                name="Description"
                placeholder="Write something.."
                style={{ height: 200 }}
                onChange={evt =>
                  this.setState({ description: evt.target.value })
                }
                value={this.state.description}
              />
              <label>title</label>
              <input
                type="text"
                placeholder="title "
                onChange={evt => this.setState({ title: evt.target.value })}
                value={this.state.title}
              />
              <label>image</label>
              <input
                type="file"
                placeholder="image "
                onChange={evt => this.setState({ image: evt.target.value })}
                //value={this.state.image}
              />
              <input
                className="col-75"
                type="submit"
                value="Submit"
                onClick={() =>
                  this.updatePost(this.state.id, {
                    dates: this.state.dates,
                    description: this.state.description,
                    title: this.state.title,
                    image: this.state.image
                  })
                }
              />
            </div>
          ) : null}
        </div>
        <div className="record">
          <button onClick={() => this.togglecreatevisibility()}>
            New Record
          </button>
          {this.state.createvisibility ? (
            <div>
              <form onSubmit={this.onSubmit}>
                <label>date</label>
                <input
                  type="text"
                  placeholder="date "
                  onChange={evt => this.setState({ dates: evt.target.value })}
                />
                <label>description</label>
                <textarea
                  id="Description"
                  name="Description"
                  placeholder="Write something.."
                  style={{ height: 200 }}
                  onChange={evt =>
                    this.setState({ description: evt.target.value })
                  }
                />
                <label>title</label>
                <input
                  type="text"
                  placeholder="title "
                  onChange={evt => this.setState({ title: evt.target.value })}
                />
                <label>image</label>
                <input
                  type="file"
                  name="filefield"
                  onChange={evt => this.setState({ image: evt.target.value })}
                />
                <input
                  className="col-75"
                  type="submit"
                  value="Submit"
                  //onClick={() => this.onSubmit()}
                />
              </form>
            </div>
          ) : null}
        </div>
        <ToastContainer />
        <span>List</span>
        {this.state.articles_list.map(blog => {
          return (
            <div>
              <table>
                <tr>
                  <td>{blog.id}</td>
                  <td>{blog.title}</td>
                  <td>{blog.description}</td>
                  <td>{blog.dates}</td>
                  <td>{blog.image}</td>
                  <td>
                    <button onClick={() => this.deletePost(blog.id)}>
                      delete
                    </button>
                  </td>
                  <td>
                    <button onClick={() => this.toggleeditvisibility(blog.id)}>
                      Edit
                    </button>
                  </td>
                </tr>
              </table>
            </div>
          );
        })}
      </div>
    );
  }
}
export default Blogdash;
