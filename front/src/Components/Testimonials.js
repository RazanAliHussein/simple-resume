import React, { Component } from 'react';

class Testimonials extends Component {
   state={
      testimonials_list:[]
   }
   componentDidMount() {
      const getList = async () => {
        const response = await fetch("http://localhost:8080/testemonial/list");
        const data = await response.json();
        this.setState({ testimonials_list: data.result });
      };
      getList();
    }
  render() {

   //  if(this.props.data){
   //    var testimonials = this.props.data.testimonials.map(function(testimonials){
   //      return  <li key={testimonials.user}>
   //          <blockquote>
   //             <p>{testimonials.text}</p>
   //             <cite>{testimonials.user}</cite>
   //          </blockquote>
   //       </li>
   //    })
   //  }

    return (
      <section id="testimonials">
      <div className="text-container">
         <div className="row">

            <div className="two columns header-col">
               <h1><span>Client Testimonials</span></h1>
            </div>

            <div className="ten columns flex-container">
                  <ul className="slides">
                     {this.state.testimonials_list.map(teste=>(
                        <li key={teste.id}>
                        <blockquote>
                           <p>{teste.description}</p>
                           <cite>{teste.author}</cite>
                        </blockquote>
                     </li> 
                     ))}
                  </ul>
               </div>
            </div>
         </div>
   </section>
    );
  }
}

export default Testimonials;
