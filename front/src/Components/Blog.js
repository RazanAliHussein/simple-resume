import React, { Component } from "react";
import { Link } from 'react-router-dom';

class Blog extends Component {
  state = {
    blog_list: []
  };
  componentDidMount() {
    const getList = async () => {
      const response = await fetch("//localhost:8080/post/list");
      const data = await response.json();
      this.setState({ blog_list: data.result });
    };
    getList();
  }

  render() {
    
    return (
      <div>
    <ul id="nav" className="nav">
    <li> <Link   to="/" >Home</Link></li>
    </ul>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
          <div
            className="collapse navbar-collapse justify-content-md-center"
            id="navbarsExample08"
          >
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link"></a>
              </li>
            </ul>
          </div>
        </nav>

        <div className="container">
          <section id="blog_area" className="pt-5">
            <div id="masonry">
              {this.state.blog_list.map(post => (
                <div className="card" key={post.id}>
                  <div className="card__img">
                    <a href="#">
                      <img
                        className="img-fluid"
                        src="https://picsum.photos/500/200/?random"
                        alt="Image 1"
                      />
                    </a>
                    <div className="card__date">
                      <span>{post.dates}</span>
                    </div>
                  </div>
                  <div className="card__content">
                    <h4 className="card__title">Card 1 Title</h4>
                    <p className="card__content-jcenter">
                     {post.description}
                    </p>
                    
                  </div>
                </div>
              ))}
            </div>
          </section>
        </div>
      </div>
    );
  }
}
export default Blog;
