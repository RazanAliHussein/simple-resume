import React, { Component } from "react";
import PickyDateTime from "react-picky-date-time";
import Popup from "reactjs-popup";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class Portfolio extends Component {
  
  state = {
    showPickyDateTime: true,
    date: '30',
    month: '01',
    year: '2000',
    hour: '03',
    minute: '10',
    second: '40',
    meridiem: 'PM',
    email:"",
    name:"",
    message:""
  };
  sendEmail(name, email, message) {
    toast("Your appointement has recorded you will get reply very soon");
    fetch(`//localhost:8080/send`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        message: this.state.message

      })
    })
      .then(res => res.json())
      .then(res => {
        console.log("here is the response: ", res);
      })
      .catch(err => {
        console.error("here is the error: ", err);
      });
  }

  onYearPicked(res) {
    const { year } = res;
    this.setState({ year: year});
  }
 
  onMonthPicked(res) {
    const { month, year } = res;
    this.setState({ year: year, month: month});
  }
 
  onDatePicked(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
 
  onResetDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
 
  onResetDefaultDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
 
  onSecondChange(res) {
    this.setState({ second: res.value });
  }
 
  onMinuteChange(res) {
    this.setState({ minute: res.value });
  }
 
  onHourChange(res) {
    this.setState({ hour: res.value });
  }
 
  onMeridiemChange(res) {
    this.setState({ meridiem: res });
  }
 
  onResetTime(res) {
    this.setState({
      second: res.clockHandSecond.value,
      minute: res.clockHandMinute.value,
      hour: res.clockHandHour.value
    });
  }
 
  onResetDefaultTime(res) {
    this.setState({
      second: res.clockHandSecond.value,
      minute: res.clockHandMinute.value,
      hour: res.clockHandHour.value
    });
  }
 
  onClearTime(res) {
    this.setState({
      second: res.clockHandSecond.value,
      minute: res.clockHandMinute.value,
      hour: res.clockHandHouemailr.value
    });
  }
 
  // just toggle your outter component state to true or false to show or hide <PickyDateTime/>
  openPickyDateTime() {
    this.setState({showPickyDateTime: true});
  }
 
  onClose() {
    this.setState({showPickyDateTime: false});
  }

  alert(){
   window.alert("test")  
  }
   myFunction() {
    console.log("test")
    let email = prompt("Please enter your email", "example@gmail.com");
    if (email != null) {
      console.log(this.state.email)
      let message="hour :"+ this.state.hour
      this.setState({message}, () => 
        this.setState({email:email}, () => 
          this.sendEmail()
        )  
      )
    }
  }
  render() {
  
    const {
      showPickyDateTime,
      date,
      month,
      year,
      hour,
      minute,
      second,
      meridiem 
    } = this.state;



    return (
      <section id="portfolio">
       <ToastContainer />
        <div className="row">
          <div className="twelve columns collapsed">
            <h1>Take an Appointement.</h1>

            <PickyDateTime
                 size="xs"
                 mode={1}
                 show={showPickyDateTime}
                 locale="en-us"
                //  onClose={() => this.setState({ showPickyDateTime: false })}
                 onYearPicked={res => this.onYearPicked(res)}
                 onMonthPicked={res => this.onMonthPicked(res)}
                 onDatePicked={res => this.onDatePicked(res)}
                 onResetDate={res => this.onResetDate(res)}
                 onSecondChange={res => this.onSecondChange(res)}
                 onMinuteChange={res => this.onMinuteChange(res)}
                 onHourChange={res => this.onHourChange(res)}
                 onMeridiemChange={res => this.onMeridiemChange(res)}
                 onResetTime={res => this.onResetTime(res)}
                 onClearTime={res => this.onClearTime(res)}
            />
            
            <button onClick={() => this.myFunction()} >Submit Appointement</button>
          </div>
        </div>
      </section>
    );
  }
}

export default Portfolio;
