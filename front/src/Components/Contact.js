import React, { Component } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Contact extends Component {
  state = {
    name: "",
    email: "",
    message: "",
    subject: ""
  };
  ///Submit function
  sendEmail(name, email, message) {
    try{
      toast("message sent");
    fetch(`//localhost:8080/send`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        message: this.state.message
      })
    })
    
      .then(res => res.json())
      .then(res => {
        console.log("here is the response: ", res);
       
      })
      
      .catch(err => {
        console.error("here is the error: ", err);
      });
    }
    catch(err){
      toast.error(err.message);
    }
  }
  //submit function end
  //handle the change
  handleChangeName=(e)=> {
    this.setState({name: e.target.value});
  }
  handleChangeEmail=(e)=>{
    this.setState({email: e.target.value});
  }
  handleChangeSubject=(e)=>{
    this.setState({subject: e.target.value});
  }
  handleChangeMessage=(e)=>{
    this.setState({message: e.target.value});
  }
  //handle the change end

  render() {
    if (this.props.data) {
      var name = this.props.data.name;
      var street = this.props.data.address.street;
      var city = this.props.data.address.city;
      var state = this.props.data.address.state;
      var zip = this.props.data.address.zip;
      var phone = this.props.data.phone;
     // var email = this.props.data.email;
      var message = this.props.data.contactmessage;
    }

    return (
      <section id="contact">
          <ToastContainer />
        <div className="row section-head">
          <div className="two columns header-col">
            <h1>
              <span>Get In Touch.</span>
            </h1>
          </div>

          <div className="ten columns">
            <p className="lead">{message}</p>
          </div>
        </div>

        <div className="row">
          <div className="eight columns">
          
              <fieldset>
                <div>
                  <label htmlFor="contactName">
                    Name <span className="required">*</span>
                  </label>
                  <input
                    type="text"
                    defaultValue=""
                    size="35"
                    id="contactName"
                    name="contactName"
                    onChange={this.handleChangeName}
                  />
                </div>

                <div>
                  <label htmlFor="contactEmail">
                    Email <span className="required">*</span>
                  </label>
                  <input
                    type="text"
                    defaultValue=""
                    size="35"
                    id="contactEmail"
                    name="contactEmail"
                    onChange={this.handleChangeEmail}
                  />
                  
                </div>

                <div>
                  <label htmlFor="contactSubject">Subject</label>
                  <input
                    type="text"
                    defaultValue=""
                    size="35"
                    id="contactSubject"
                    name="contactSubject"
                    onChange={this.handleChangeSubject}
                  />
               
                </div>

                <div>
                  <label htmlFor="contactMessage">
                    Message <span className="required">*</span>
                  </label>
                  <textarea
                    cols="50"
                    rows="15"
                    id="contactMessage"
                    name="contactMessage"
                    onChange={this.handleChangeMessage}
                  />
                </div>

                <div>
                  <button className="submit" onClick={() => this.sendEmail()} >
                    Submit
                  </button>
                  <span id="image-loader">
                    <img alt="" src="images/loader.gif" />
                  </span>
                </div>
              </fieldset>
            {/* </form> */}

            <div id="message-warning"> Error boy</div>
            <div id="message-success">
              <i className="fa fa-check" />Your message was sent, thank you!
              <br />
            </div>
          </div>

          <aside className="four columns footer-widgets">
            <div className="widget widget_contact">
              <h4>Address and Phone</h4>
              <p className="address">
                {name}
                <br />

                {city}
                <br />
                <span>{phone}</span>
              </p>
            </div>
          </aside>
        </div>
      </section>
    );
  }
}

export default Contact;
